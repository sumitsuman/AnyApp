//
//  TokenSession.swift
//  AniAppDemo
//
//  Created by Sumit Suman on 15/11/17.
//  Copyright © 2017 Sumit Suman. All rights reserved.
//

import Foundation

class TokenSession {
    
    static let sharedInstance = TokenSession()
    var accessToken: String?
}
