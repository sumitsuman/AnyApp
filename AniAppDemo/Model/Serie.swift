//
//  Serie.swift
//  AniAppDemo
//
//  Created by Sumit Suman on 15/11/17.
//  Copyright © 2017 Sumit Suman. All rights reserved.
//

import Foundation
import ObjectMapper

class Serie: Mappable {
    
    var id: Int?
    var title: String?
    var thumbImageUrl: String?
    var imageUrl: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title_english"]
        thumbImageUrl <- map["image_url_med"]
        imageUrl <- map["image_url_lge"]
    }
}

