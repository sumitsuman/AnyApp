//
//  Episode.swift
//  AniAppDemo
//
//  Created by Sumit Suman on 15/11/17.
//  Copyright © 2017 Sumit Suman. All rights reserved.
//

import Foundation
import ObjectMapper

class Episode: Mappable {
    
    var id: Int?
    var name: String?
    var description: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
    }
}

