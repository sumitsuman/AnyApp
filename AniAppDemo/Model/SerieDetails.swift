//
//  SerieDetails.swift
//  AniAppDemo
//
//  Created by Sumit Suman on 15/11/17.
//  Copyright © 2017 Sumit Suman. All rights reserved.
//

import Foundation
import ObjectMapper

class SerieDetails: Mappable {
    
    var id: Int?
    var description: String?
    var characters: [CharacterDetails]?
    var episodes: [Episode]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        description <- map["description"]
        characters <- map["characters"]
        episodes <- map["tags"]
    }
}

