//
//  SearchModel.swift
//  AniAppDemo
//
//  Created by Sumit Suman on 15/11/17.
//  Copyright © 2017 Sumit Suman. All rights reserved.
//

import Foundation
import ObjectMapper


class SearchModel: Mappable {
    var id: Int?
    var title: String?
    var bayesianAverage: Double?
    var imageRemotePath: String?
    var plotSummary: String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title_english"]
        bayesianAverage <- map["bayesianAverage"]
        imageRemotePath <- map["image_url_lge"]
        plotSummary <- map["plotSummary"]
    }

}
